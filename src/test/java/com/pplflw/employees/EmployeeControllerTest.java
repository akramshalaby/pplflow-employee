/**
 * 
 */
package com.pplflw.employees;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pplflw.data.Employee;
import com.pplflw.data.EmployeeState;
import com.pplflw.service.EmployeeService;


/**
 * @author akramsh
 *
 */
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(
		  locations = "classpath:application.properties")
public class EmployeeControllerTest {

	@Autowired
	private MockMvc mvc;

	
	@Autowired
	EmployeeService empService;
	
	
	
	@Test
	public void testPostEmployee() throws Exception{
		Employee emp = new Employee();
		emp.setFirstName("akram");
		emp.setAge(44);
		mvc.perform(MockMvcRequestBuilders.post("/employees")
				.content(asJson(emp))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
				.andDo(print());
	}

	 
	@Test
	public void testGetEmployeeByIdAPI() throws Exception 
	{
		Employee emp = createAndPostTestEmployee();
	  mvc.perform( MockMvcRequestBuilders
	      .get("/employees/{id}", 1)
	      .accept(MediaType.APPLICATION_JSON))
	      .andDo(print())
	      .andExpect(status().isOk())
	      .andExpect(MockMvcResultMatchers.jsonPath("$.age" ).value(emp.getAge()));
	}
	
	@Test
	public void testDeleteEmployeeById() throws Exception {
		Employee emp = createAndPostTestEmployee();
		mvc.perform(MockMvcRequestBuilders.delete("/employees/{id}",emp.getId()))
		.andExpect(status().isOk());
	}
	
	@Test
	public void testChangeStateSuccessForEmployee() throws Exception {
		Employee emp = createAndPostTestEmployee();
		emp.setState(EmployeeState.APPROVED);
		mvc.perform(MockMvcRequestBuilders.put("/employees/{id}", emp.getId()).content(asJson(emp))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	
	


	@Test
	public void testCheckStateFailureForEmployee() throws Exception {
		Employee emp = createAndPostTestEmployee();
		mvc.perform(MockMvcRequestBuilders.put("/employees/{id}", Instant.now().getEpochSecond()).content(asJson(emp))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().is(HttpStatus.NOT_FOUND.value()));
	}
	
	
	public static String asJson(final Object obj) {
		try {
	        return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}
	
	public static Employee asObject(String json) {
		Employee emp = null;
		try {
			emp =  new ObjectMapper().readValue(json, Employee.class);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		return emp;
	}
	

	private Employee createAndPostTestEmployee()  throws Exception{
		Employee emp = new Employee();
		emp.setFirstName("akram");
		emp.setEmail("akramsh@gmail.com");
		String json = mvc.perform(MockMvcRequestBuilders.post("/employees")
				.content(asJson(emp))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
				.andReturn().getResponse().getContentAsString();
		emp = asObject(json);
		return emp;
	}
	

}
