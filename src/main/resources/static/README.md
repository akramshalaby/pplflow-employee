

PeopleFlow (www.pplflw.com) is a global HR platform enabling companies to hire & onboard their employees internationally, at the push of a button. It is our mission to create opportunities for anyone to work from anywhere. As work is becoming even more global and remote, there has never been a bigger chance to build a truly global HR-tech company.


As a part of our backend engineering team, you will be responsible for building our core platform including an  employees managment system.

The employees on this system are assigned to different states, Initially when an employee is added it will be assigned "ADDED" state automatically .


The other states (State machine) for A given Employee are:
- ADDED
- IN-CHECK
- APPROVED
- ACTIVE

Our backend stack is:
- Java 11 
- Spring Framework 
- Kafka


**First Part:**

Employee Controller handles the add, change, delete and findById operations (produces and consumes JSON objects)
State machine currently runs of public enum EmployeeStatus


Please provide a solution with the  above features with the following consideration.

- To run the service in docker daemon:
docker pull akramsh/pplflw:v001 (pushed to dockerHub public repo for now)
docker run --name pplflw-employees -p 8005:8005 akramsh/pplflw:v001

- For state machine:
Currently run from a public enum

- Testing:
Test for the http layer operations included

- Providing an API Contract e.g. OPENAPI spec. is a big plus
included swagger2 documentation that is open for improvement http://pplflw-employees:8005/swagger-ui.html





**Second Part (Optional but a plus):**

Being concerned about developing high quality, resilient software, giving the fact, that you will be participating, mentoring other engineers in the coding review process.


Suggest what will be your silver bullet, concerns while you're reviewing this part of the software that you need to make sure is being there.
The rule of thumb is that "there is no silver bullet". Yet, I would look for:
	
*   function names
*   usage of persistence layer
*	retries of external resources / services
*	exception handling in corner cases
*	Complex code blocks is usually a candidate for early review

- What the production-readiness criteria that you consider for this solution
*	Remove internal assumptions (currently enum instead of calling a service)
*	Logging is adequately added
*	Health check end-points are tested and within acceptable metrics (db-queries / end-points response times)






**Third Part (Optional but a plus):**
Another Team in the company is building another service, This service will be used to provide some statistics of the employees, this could be used to list the number of employees per country, other types of statistics which is very vague at the moment.

Please think of a solution without any further implementation that could be able to integrate on top of your service, including the integration pattern will be used, the database storage etc.
A high-level architecture diagram is sufficient to present this.

The given information about the other solution is vague, yet the givens are:
* statistics service
* makes use of the current CRUD service

I would suggest the "other" service - can be named pplflw-stats - to be:
* fed updates from the current service
* de-coupled from this service, not to overwhelm performance of this service

This leads to me suggesting:
* pplflw-employee service sends updates of actions to kafka topic
* pplflw-stats:
	- receives updates from kafka
	- stores updates to olap data store
	- stores updated stats (KSQL and stream processing) to olap data store
* pplflw-stats-present:
	- respond to queries from olap data store (ui, raw-queries, http-layer)
* pplflw-stats-present: can be an ELK stack (i.e. elasticSearck, Logstash and Kibana)
https://aws.amazon.com/elasticsearch-service/the-elk-stack/
	




