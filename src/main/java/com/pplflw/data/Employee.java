/**
 * 
 */
package com.pplflw.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author akramsh
 *
 */

@Data
@NoArgsConstructor
@Entity
public class Employee {
	
	@Id
	@GeneratedValue
	@ApiModelProperty(required = false)
	private Long id;
	private String firstName;
	private String lastName;
	private Integer age;
	private String email;
	private EmployeeState state = EmployeeState.ADDED;

}
