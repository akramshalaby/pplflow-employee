/**
 * 
 */
package com.pplflw.data;

/**
 * @author akramsh
 *
 */
public enum EmployeeState {
	ADDED, IN_CHECK, APPROVED, ACTIVE
}
