/**
 * 
 */
package com.pplflw.service;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.pplflw.data.Employee;

/**
 * @author akramsh
 *
 */

@Service
public class EmployeeService {
	
	private EmployeeRepository repo;
	
	public EmployeeService(EmployeeRepository repo) {
		this.repo = repo;
	}

	public Employee addEmployee(Employee employee) throws Exception{
		employee = repo.save(employee); //TODO: check existence of key data
		return employee;
	}
	
	public Employee getEmpById(Long id) throws Exception{
		Employee emp =  repo.findById(id).orElse(null);
		if(emp == null) 
			throw new Exception(String.format("Employee#%d is NOT_FOUND", id));
		else
			return emp;
	}


	public Employee updateEmployee(Employee newEmp, Long id) throws Exception{
		return repo.findById(id)
			.map(emp -> {
				emp.setAge(newEmp.getAge()==null ? emp.getAge() : newEmp.getAge());
				emp.setEmail(newEmp.getEmail()==null ? emp.getEmail(): newEmp.getEmail());
				emp.setState(newEmp.getState()==null ? emp.getState() : newEmp.getState());
				emp.setFirstName(newEmp.getFirstName()==null ? emp.getFirstName() : newEmp.getFirstName());
				emp.setLastName(newEmp.getLastName()==null ? emp.getLastName() : newEmp.getLastName());
				return repo.save(newEmp);
			}).orElseThrow(()-> new Exception("NOT_FOUND"));  //you can decide later to add newEmp and NOT being strict update
	}


	public Boolean delEmpById(Long id) throws Exception {
		Employee emp = repo.findById(id).orElseThrow(() -> new Exception("NOT_FOUND"));
		repo.delete(emp);
		return true;
	}

	/**
	 * @return
	 */
	public List<Employee> getAllEmployees() {
		return repo.findAll();
	}

}

@Repository
interface EmployeeRepository extends  JpaRepository<Employee,Long>{}
