package com.pplflw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PplFlwApplication {

	public static void main(String[] args) {
		SpringApplication.run(PplFlwApplication.class, args);
	}

}
