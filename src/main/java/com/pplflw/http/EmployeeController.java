/**
 * 
 */
package com.pplflw.http;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pplflw.data.Employee;
import com.pplflw.service.EmployeeService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

/**
 * @author akramsh
 *
 */
@RestController
@RequestMapping("employees")
@Slf4j
public class EmployeeController {
	
	private EmployeeService empService;
	
	@Autowired
	public EmployeeController(EmployeeService empService) {
		this.empService = empService;
	}

	
	@GetMapping
	public List<Employee> getAllEmployees(){
		return  empService.getAllEmployees();
	}
	
	@PostMapping
	@ApiOperation(value = "add new employee", notes = "id auto-generated")
	@ApiResponses(value = {@ApiResponse(code= 200, message = "added"),
			@ApiResponse(code=409, message = "User exists")})
	public ResponseEntity<Employee> postEmployee(@RequestBody Employee employee) {
		try {
			return ResponseEntity.ok().body(empService.addEmployee(employee));
		} catch (Exception e) {
			log.error(String.format("postEmployee: could NOT add %s with exception %s", employee.toString(), e));
			return ResponseEntity.status(HttpStatus.CONFLICT).build(); 
		}
	}
	
	@PutMapping("{id}")
	public ResponseEntity<Employee> putEmployee(@RequestBody Employee employee, @PathVariable Long id){
		try {
			return ResponseEntity.ok().body(empService.updateEmployee(employee, id));
		} catch (Exception e) {
			log.error(String.format("putEmployee: could NOT update %s with exception %s", employee.toString(), e));
			//return postEmployee(employee); //TODO: decision to merge if employee not found
			return ResponseEntity.notFound().build();
		}
	}
	
	@GetMapping("{id}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable Long id ){
		try {
			return ResponseEntity.ok().body(empService.getEmpById(id));
		} catch(Exception e) {
			log.warn(String.format("getEmployeeById: exception: %s", e)); //TODO: remote error handler here
			return ResponseEntity.notFound().build();
		}
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<Boolean> deleteEmployee(@PathVariable Long id){
		try {
			return ResponseEntity.ok().body(empService.delEmpById(id));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(false);
		}
	}
}
