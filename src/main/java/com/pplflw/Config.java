package com.pplflw;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author akramsh
 *
 */

@Configuration
@EnableSwagger2
public class Config {
	

	
	@Bean
	public Docket getSwaggerDocket() {
		
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
				.build()
				.apiInfo(apiEndPointsInfo())
				.useDefaultResponseMessages(false);
		
	}

	private ApiInfo apiEndPointsInfo() {
		
        return new ApiInfoBuilder().title("People Flow Employee API")
                .description("People Flow Employee API")
                .contact(new Contact("akramsh", "https://linkedin.com/in/akramsh/", "akramsh@gmail.com"))
                .license("Apache License 2.0")
                .licenseUrl("https://linkedin.com/in/akramsh/")
                .version("v001")
                .build();

        }
	

}
